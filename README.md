# AutoConfig Point2Site OpenVPN

Automaticly configure OpenVPN 2.4 + Easy-RSA 3 + FirewallD on Ubuntu (20.04) to create a Point to Site VPN with a bundled ovpn certificated file by user.

WARNING: This script will modify interfaces zones in FirewallD so don't use it in production environement with other apps using network.

First time prepare the server by editing the vars on file HEADER:
```
#EASY RSA
COUNTRY="FR"
PROVINCE="IDF"
CITY="PARIS"
ORG="ORGANISATION"
EMAIL="adresse@e.mail"
OU="Entite"

#OPENVPN
LISTEN_FQDN="rebond.fqdn"
LISTEN_IP="x.x.x.x"
LISTEN_PORT="1194"
TUNNEL_NET="10.254.254.0"
TUNNEL_MASK="255.255.255.0"
ENCAPSULED_NET="10.0.0.0"
ENCAPSULED_MASK="255.255.0.0"
```

Then use:
```
root@rebond:~# ./P2S-init.sh server
## Start OpenVPN & Easy RSA Installation ##
## OpenVPN & Easy RSA Installed ##
## Start Easy RSA Configuration ##
## End Easy RSA Configuration ##
## Start OpenVPN Configuration ##
## End OpenVPN Configuration ##
## Start Firewalld Configuration ##
## End Firewalld Configuration ##
```

After you can crete users like: 
```
root@rebond:~# ./P2S-init.sh Sylvain Laurent
## OpenVPN Client Certificate Generation ##
## End OpenVPN Client Certificate Generation ##
## Create Client Config File ##
## End Create Client Config File ##
## OpenVPN Client Certificate Generation ##
## End OpenVPN Client Certificate Generation ##
## Create Client Config File ##
## End Create Client Config File ##
root@rebond:~# ll *.ovpn
-rw-r--r-- 1 root root 24814 Oct 18 06:15 Laurent.ovpn
-rw-r--r-- 1 root root 24813 Oct 18 06:15 Sylvain.ovpn
```

Or you can combine:
```
root@rebond:~# ./P2S-init.sh server Sylvain Laurent
## Start OpenVPN & Easy RSA Installation ##
## OpenVPN & Easy RSA Installed ##
## Start Easy RSA Configuration ##
## End Easy RSA Configuration ##
## Start OpenVPN Configuration ##
## End OpenVPN Configuration ##
## Start Firewalld Configuration ##
## End Firewalld Configuration ##
## OpenVPN Client Certificate Generation ##
## End OpenVPN Client Certificate Generation ##
## Create Client Config File ##
## End Create Client Config File ##
## OpenVPN Client Certificate Generation ##
## End OpenVPN Client Certificate Generation ##
## Create Client Config File ##
## End Create Client Config File ##
root@rebond:~# ll *.ovpn
-rw-r--r-- 1 root root 24814 Oct 18 06:15 Laurent.ovpn
-rw-r--r-- 1 root root 24813 Oct 18 06:15 Sylvain.ovpn
```

Then on client you use:
```
sudo openvpn --config Sylvain.ovpn
```

Want SSH is OK other VPN use:
```
firewall-cmd --permanent --zone=public --remove-service=ssh
```

I suggest to protect your VPN by using Fail2ban (https://www.fail2ban.org) with: https://gist.github.com/drmalex07/463e4c7356bcfb2b3d21ff9fdc5aa6b3
I suggest to protect your VPN server with RKHUNTER: http://rkhunter.sourceforge.net/

WARNING: if you want to use "network-manager-openvpn-gnome":
after importing file in it select "use this connection only for resources on its network" or every traffic would pass through the VPN (no more internet with this script)
- Gnome BUG: https://gitlab.gnome.org/GNOME/NetworkManager-openvpn/-/issues/48
