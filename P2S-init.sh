#!/bin/bash

LOG_FILE="/var/log/P2S-init.log"
DIR="${PWD}"
    
#EASY RSA
COUNTRY="FR"
PROVINCE="IDF"
CITY="PARIS"
ORG="ORGANISATION"
EMAIL="adresse@e.mail"
OU="Entite"

#OPENVPN
LISTEN_FQDN="rebond.fqdn"
LISTEN_IP="x.x.x.x"
LISTEN_PORT="1194"
TUNNEL_NET="10.254.254.0"
TUNNEL_MASK="255.255.255.0"
ENCAPSULED_NET="10.0.0.0"
ENCAPSULED_MASK="255.255.0.0"

function server {
    echo "## Start OpenVPN & Easy RSA Installation ##" | tee -a ${LOG_FILE}
    apt update >> ${LOG_FILE} 2>&1
    apt install -y openvpn easy-rsa firewalld >> ${LOG_FILE} 2>&1
    echo "## OpenVPN & Easy RSA Installed ##" | tee -a ${LOG_FILE}

    echo "## Start Easy RSA Configuration ##" | tee -a ${LOG_FILE}
    make-cadir /etc/openvpn/rsa >> ${LOG_FILE} 2>&1
    cd "/etc/openvpn/rsa" 2>/dev/null || { echo "ERROR: Can acces Easy RSA Directory"; exit 3; }
    sed -i 's!#set_var EASYRSA_REQ_COUNTRY.*$!set_var EASYRSA_REQ_COUNTRY\t'"${COUNTRY}"'!g' vars
    sed -i 's!#set_var EASYRSA_REQ_PROVINCE.*$!set_var EASYRSA_REQ_PROVINCE\t'"${PROVINCE}"'!g' vars
    sed -i 's!#set_var EASYRSA_REQ_CITY.*$!set_var EASYRSA_REQ_CITY\t'"${CITY}"'!g' vars
    sed -i 's!#set_var EASYRSA_REQ_ORG.*$!set_var EASYRSA_REQ_ORG\t\t'"${ORG}"'!g' vars
    sed -i 's!#set_var EASYRSA_REQ_EMAIL.*$!set_var EASYRSA_REQ_EMAIL\t'"${EMAIL}"'!g' vars
    sed -i 's!#set_var EASYRSA_REQ_OU.*$!set_var EASYRSA_REQ_OU\t\t'"${OU}"'!g' vars
    sed -i 's!#set_var EASYRSA_BATCH.*$!set_var EASYRSA_BATCH\t\t"enabled"!g' vars
    ./easyrsa init-pki >> ${LOG_FILE} 2>&1
    touch pki/.rnd
    export RANDFILE="/etc/openvpn/rsa/pki/.rnd"
    { ./easyrsa build-ca nopass
    ./easyrsa gen-dh
    ./easyrsa build-server-full "${LISTEN_FQDN}" nopass; } >> ${LOG_FILE} 2>&1
    cp -f "/etc/openvpn/rsa/pki/ca.crt" "/etc/openvpn/server/ca.crt"
    cp -f "/etc/openvpn/rsa/pki/dh.pem" "/etc/openvpn/server/dh2048.pem"
    cp -f "/etc/openvpn/rsa/pki/issued/${LISTEN_FQDN}.crt" "/etc/openvpn/server/server.crt"
    cp -f "/etc/openvpn/rsa/pki/private/${LISTEN_FQDN}.key" "/etc/openvpn/server/server.key"
    openvpn --genkey --secret /etc/openvpn/server/ta.key >> ${LOG_FILE} 2>&1
    echo "## End Easy RSA Configuration ##" | tee -a ${LOG_FILE}


    echo "## Start OpenVPN Configuration ##" | tee -a ${LOG_FILE}
    gunzip /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz -c > /etc/openvpn/server/server.conf
    sed -i 's!^;local.*$!local '"${LISTEN_IP}"'!g' /etc/openvpn/server/server.conf
    sed -i 's!^port.*$!port '"${LISTEN_PORT}"'!g' /etc/openvpn/server/server.conf
    sed -i 's!^;proto tcp!proto tcp4!g' /etc/openvpn/server/server.conf
    sed -i 's!^proto udp!;proto udp!g' /etc/openvpn/server/server.conf
    sed -i 's!^explicit-exit-notify!;explicit-exit-notify!g' /etc/openvpn/server/server.conf
    sed -i 's!server 10.8.0.0 255.255.255.0!server '"${TUNNEL_NET}"' '"${TUNNEL_MASK}"'!g' /etc/openvpn/server/server.conf
    sed -i 's!;push "route 192.168.20.0 255.255.255.0"!push "route '"${ENCAPSULED_NET}"' '"${ENCAPSULED_MASK}"'"!g' /etc/openvpn/server/server.conf
    sed -i 's!;user nobody!user nobody!g' /etc/openvpn/server/server.conf
    sed -i 's!;group nogroup!group nogroup!g' /etc/openvpn/server/server.conf
    systemctl enable --now openvpn-server@server.service >> ${LOG_FILE} 2>&1
    echo "## End OpenVPN Configuration ##" | tee -a ${LOG_FILE}


    echo "## Start Firewalld Configuration ##" | tee -a ${LOG_FILE}
    #ALLOW ROUTING
    echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/enable_ip_forwarding.conf
    echo "net.ipv6.conf.all.forwarding=1" >> /etc/sysctl.d/enable_ip_forwarding.conf
    sysctl -w net.ipv4.ip_forward=1 >> ${LOG_FILE} 2>&1
    sysctl -w net.ipv6.conf.all.forwarding=1 >> ${LOG_FILE} 2>&1
    #DISCOVER INTERFACES
    pub_int="$(ip r | grep "link src ${LISTEN_IP}" | awk '{print $3}')"
    priv_int="$(ip r | grep "^${ENCAPSULED_NET}" | awk '{print $3}')"
    #MODIFYING SERVICE
    cp /usr/lib/firewalld/services/openvpn.xml /etc/firewalld/services/
    sed -i 's/udp/tcp/g' /etc/firewalld/services/openvpn.xml
    sed -i 's/1194/'"${LISTEN_PORT}"'/g' /etc/firewalld/services/openvpn.xml
    #ACTIVATE FIREWALL
    { systemctl enable --now firewalld.service
    systemctl reload firewalld.service
    #ADD OPENVPN EN PUBLIC INTERFACE ZONE
    firewall-cmd --permanent --zone=public --add-interface="${pub_int}"
    firewall-cmd --permanent --zone=public --add-service=openvpn
    #ADD TUNNEL AND PRIVATE ZONE IN TRUSTED ZONE AND ALLOW MASQUARADE
    firewall-cmd --permanent --zone=trusted --add-interface="${priv_int}"
    firewall-cmd --permanent --zone=trusted --add-interface="tun0"
    firewall-cmd --permanent --zone=trusted --add-masquerade
    systemctl reload firewalld.service >> ${LOG_FILE} 2>&1
    } >> ${LOG_FILE} 2>&1
    echo "## End Firewalld Configuration ##" | tee -a ${LOG_FILE}
}

function client {
    local USER="$1"

    echo "## OpenVPN Client Certificate Generation ##" | tee -a ${LOG_FILE}
    cd "/etc/openvpn/rsa/" 2>/dev/null || { echo "ERROR: Can create user whitout server, try $0 server"; exit 4; }
    ./easyrsa build-client-full "${USER}" nopass >> ${LOG_FILE} 2>&1
    echo "## End OpenVPN Client Certificate Generation ##" | tee -a ${LOG_FILE}

    echo "## Create Client Config File ##" | tee -a ${LOG_FILE}
    cd "${DIR}" || exit 5
    { echo "client"
    echo "dev tun"
    echo "proto tcp"
    echo "remote ${LISTEN_FQDN} ${LISTEN_PORT}"
    echo "resolv-retry infinite"
    echo "nobind"
    echo "persist-key"
    echo "persist-tun"
    echo "tls-auth ta.key 1"
    echo "tun-mtu 1500"
    echo "remote-cert-tls server"
    echo "cipher AES-256-CBC"
    echo "auth-nocache"
    echo "verb 3"
    echo "<ca>"
    cat "/etc/openvpn/server/ca.crt"
    echo "</ca>"
    echo "<cert>"
    cat "/etc/openvpn/rsa/pki/issued/${USER}.crt"
    echo "</cert>"
    echo "<key>"
    cat "/etc/openvpn/rsa/pki/private/${USER}.key"
    echo "</key>"
    echo "<tls-auth>"
    cat "/etc/openvpn/server/ta.key"
    echo "</tls-auth>"; } >> "${USER}.ovpn"
    echo "## End Create Client Config File ##" | tee -a ${LOG_FILE}
}

#### MAIN ####

#Validate Root
if [ ${EUID} -ne 0 ]
then
	echo "Need to be root"
	exit 1
fi

#Validate distrib
if ! grep -q "DISTRIB_ID=Ubuntu" "/etc/lsb-release"
then
	echo "Script made for Ubuntu"
	exit 2
fi

for arg in "$@"
do
    if [ "${arg}" == "server" ]
    then
        server
    else
        client "${arg}"
    fi
done

exit 0
